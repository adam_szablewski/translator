import pytest

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator


# testuje translację dla metody bez wielkich liter
@pytest.mark.parametrize("test_input, expected", [
    ("ala", "gug"),
    ("gala", "agug"),
    ("kot", "ipt"),
    ("wonsz", "wpnsz"),
    ("lok", "upi"),
    ("LOK", "LOK"),
    ("qwc", "qwc"),
    ("r", "y"),
    ("KoT", "KpT"),


])
def test_should_translate(translator, test_input, expected):
    assert translator.translate(test_input) == expected


def translator2():
    translator = GaDeRyPoLuKi()
    return translator


# testuje translację dla metody z wielkimi literami
@pytest.mark.parametrize("test_input, expected", [
    ("ALA", "gug"),
    ("GaLa", "agug"),
    ("KoT", "ipt"),
    ("wonsz", "wpnsz"),
    ("lok", "upi"),
    ("LOK", "upi"),
    ("QWC", "qwc"),
    ("R", "y"),
    ("KoT", "ipt"),


])
def test_should_translate2(translator, test_input, expected):
    assert translator.translate_ignore_case(test_input) == expected


def translator3():
    translator = GaDeRyPoLuKi()
    return translator


# testuje czy znaki z poza zdefiniowanego zakresu zostaną nieprzetłumaczone
@pytest.mark.parametrize("test_input, expected", [
    ("ALA", "ALA"),
    ("GaLa", "GgLg"),
    ("KoT", "KpT"),
    ("wOnsz", "wOnsz"),
    ("lók", "uói"),
    ("ŁÓK", "ŁÓK"),
    ("ŁÓk", "ŁÓi"),
    ("R", "R"),
    ("KoT", "KpT"),


])
def test_should_not_translate(translator, test_input, expected):
    assert translator.translate(test_input) == expected


def translator4():
    translator = GaDeRyPoLuKi()
    return translator


# testuje metodę sprawdzającą czy da się przetłumaczyć znak
@pytest.mark.parametrize("test_input, expected", [
    ("a", True),
    ("ą", False),
    ("K", False),
    ("g", True),


])
def test_is_translatable(translator, test_input, expected):
    assert translator.is_translatable(test_input) == expected


def translator5():
    translator = GaDeRyPoLuKi()
    return translator


# testuje metodę podającą długość kodu
@pytest.mark.parametrize("expected", [
    (12),


])
def test_get_code_length(translator, expected):
    assert translator.get_code_length() == expected
