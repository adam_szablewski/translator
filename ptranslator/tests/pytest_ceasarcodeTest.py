# To jest miejsce od którego należy zacząć zadanie TDD
import pytest
from pytest import fail

from ceasar_code.ceasarcode import CeasarCode

# zbiór testów w postaci skryptu pytest

# fixture, czyli wcześniej przygotowany kawałek kodu do wielokrotnego uzycia
@pytest.fixture
def translator():
    translator = CeasarCode()
    return translator

# test właściwy
def test_should_translate(translator):
    # given
    msg = "lok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "ybx"

# uwaga na klauzulę wyłączającą test


def test_should_translate2(translator):
    # given
    msg = "LOK"

    # when
    result = translator.translate_ignore_case(msg)

    # then
    assert result == "ybx"


def test_should_translate3(translator):
    # given
    msg = "PiEs"

    # when
    result = translator.translate_ignore_case(msg)

    # then
    assert result == "cvrf"


def test_should_stay_not_translated(translator):
    # given
    msg = "żółć"

    # when
    result = translator.translate(msg)

    # then
    assert result == "żółć"


def test_should_translate4(translator):
    # given
    msg = "r"

    # when
    result = translator.translate(msg)

    # then
    assert result == "e"


def test_should_throw_exception(translator):
    # given

    # when
    with pytest.raises(Exception):
        translator.translate(None)

    # then


def test_should_stay_not_translated2(translator):
    # given
    msg = "KOT"

    # when
    result = translator.translate(msg)

    # then
    assert result == "KOT"


def test_should_check_not_translatable(translator):
    # given
    c = "ą"

    # when
    result = translator.is_translatable(c)

    # then
    assert not result


def test_should_check_translatable(translator):
    # given
    c = "g"

    # when
    result = translator.is_translatable(c)

    # then
    assert result


def test_should_check_code_length(translator):
    # given

    # when
    size = translator.get_code_length()

    # then
    assert size == 26