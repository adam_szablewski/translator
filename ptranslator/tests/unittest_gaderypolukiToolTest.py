import unittest

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


class GaDeRyPoLuKiToolTest(unittest.TestCase):
    g = None

    def setUp(self):
        self.g = GaDeRyPoLuKi()
        pass

    def tearDown(self):
        self.g = None
        pass

    # testuje translację dla metody bez wielkich liter
    test_cases = [
        ("ala", "gug"),
        ("gala", "agug"),
        ("KoT", "KpT"),
        ("wonsz", "wpnsz"),
    ]

    def test_should_translate(self):
        for msg, expected in self.test_cases:
            with self.subTest(name=str(msg)):
                result = self.g.translate(msg)
                self.assertEqual(result, expected)

    # testuje translację dla metody z wielkimi literami
    test_cases2 = [
        ("ALA", "gug"),
        ("GaLa", "agug"),
        ("KoT", "ipt"),
        ("wonsz", "wpnsz"),
    ]

    def test_should_translate2(self):
        for msg, expected in self.test_cases2:
            with self.subTest(name=str(msg)):
                result = self.g.translate_ignore_case(msg)
                self.assertEqual(result, expected)

    # testuje metodę sprawdzającą czy da się przetłumaczyć znak
    test_cases3 = [
        ("a", True),
        ("A", False),
        ("ą", False),
        ("g", True),
    ]

    def test_is_translatable(self):
        for msg, expected in self.test_cases3:
            with self.subTest(name=str(msg)):
                result = self.g.is_translatable(msg)
                self.assertEqual(result, expected)

    # testuje metodę podającą długość kodu
    test_cases4 = [
        12,
    ]

    def test_get_code_length(self):
        for expected in self.test_cases4:
            with self.subTest(name=int()):
                result = self.g.get_code_length()
                self.assertEqual(result, expected)