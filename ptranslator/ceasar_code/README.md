##1. Szyfr Cezara
###Założenia
* Translator implementuje szyfr Cezara w klasycznej wersji - ROT13
* Translator szyfruje jedynie znaki z alfabetu łacińskiego - testy sprawdzają również czy nie zostaną przetłumaczone znaki z poza zakresu
* W celu translacji ciągu znaków zawierających wielkie litery powinna zostać użyta metoda 'translate_ignore_case'
###Dodatkowe informacje
* Testy jednostkowe znajdują się w: \ptranslator\tests\pytest_ceasarcodeTest.py 
##2. Tools
* Testy jednostkowe w narzędziu pytest: \ptranslator\tests\pytest_gaderypolukiToolTest.py
* Testy jednostkowe w narzędziu unittest \ptranslator\tests\unittest_gaderypolukiToolTest.py